/**
 * Created by gehendrakarmacharya on 5/20/17.
 */

(function() {

    var typed_word = document.getElementById('type_word');
    var typed_number= document.getElementById('type_number');
    var result = document.getElementById('output');

    typed_word.addEventListener('keyup', generateOutput);
    typed_number.addEventListener('keyup', generateOutput);

    function generateOutput() {
        var output = "";
        output = "<h2>Output:</h2>";
        output += "<ul>";
        if (typed_number.value !== "") {
            for(var i=0; i<+typed_number.value; i++) {
                output += "<li>" + typed_word.value + "</li>";
            }
        }
        output += "</ul>";
        result.innerHTML = output;
    }

})();

